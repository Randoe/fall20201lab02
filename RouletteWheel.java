import java.util.Random;
public class RouletteWheel
{
    private Random rand;
    private int lastSpin;

    public RouletteWheel()
    {
        this.rand = new Random();
        this.lastSpin = 0;
    }

    public void spin() 
    {
        final int MAX_NUM = 37;
        this.lastSpin = rand.nextInt(MAX_NUM);
    }

    public int getValue() 
    {
        return this.lastSpin;
    }
}